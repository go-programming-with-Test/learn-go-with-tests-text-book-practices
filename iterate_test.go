// tests using benchmarks by accepting many amount of variables
// when the nuber increases and become complicated we use sieve of eratosthenes method to make our program faster
package former

import (
	"fmt"
	"testing"
)

var table = []struct {
	input int
}{
	{input: 100},
	{input: 1000},
	{input: 74382},
	{input: 382399},
}

func BenchmarkPrimeNumbers(b *testing.B) {
	for _, v := range table {
		b.Run(fmt.Sprintf("input_size_%d", v.input), func(b *testing.B) {
			for i := 0; i < b.N; i++ {
				table = append(table)
			}
		})
	}
}

// use < go test -bench="." > to run the benchmarking test program
