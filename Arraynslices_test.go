/* DeepEqual Two values of identical type are deeply equal if one of the following cases applies
+ Array values are deeply equal when their corresponding elements are deeply equal.
+ Struct values are deeply equal if their corresponding fields, both exported and unexported, are deeply equal.
+Func values are deeply equal if both are nil; otherwise they are not deeply equal.
+ Interface values are deeply equal if they hold deeply equal concrete values.
+ Map values are deeply equal when all of the following are true: they are both nil or both non-nil, they have the same length, and either they are the same map object or their corresponding keys (matched using Go equality) map to deeply equal values.
+ Pointer values are deeply equal if they are equal using Go's == operator or if they point to deeply equal values.
+ Slices are deeply equal in a condition of both are nil or not nil
https://pkg.go.dev/reflect#DeepEqual
*/

package former

import (
	"reflect" // this import help to check a func of DeepEqual
	"testing"
)

func TestSum(t *testing.T) {

	t.Run("collections of any size", func(t *testing.T) {

		numbers := []int{1, 2, 3}

		got := Sum(numbers)
		want := 6

		if got != want {
			t.Errorf("got %d want %d given, %v", got, want, numbers)
		}
	})

}

func TestSumAllTails(t *testing.T) {

	checkSums := func(t *testing.T, got, want []int) {
		if !reflect.DeepEqual(got, want) {
			t.Errorf("got %v want %v", got, want)
		}
	}

	t.Run("make the sums of tails of", func(t *testing.T) {
		got := SumAllTails([]int{1, 2}, []int{0, 9})
		want := []int{2, 9}
		checkSums(t, got, want)
	})

	t.Run("safely sum empty slices", func(t *testing.T) {
		got := SumAllTails([]int{}, []int{3, 4, 5})
		want := []int{0, 9}
		checkSums(t, got, want)
	})

}
